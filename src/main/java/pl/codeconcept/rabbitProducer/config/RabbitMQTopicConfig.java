package pl.codeconcept.rabbitProducer.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQTopicConfig {

    @Bean
    Queue teksas1Queue() {
        return new Queue("teksas1Queue", false);
    }

    @Bean
    Queue teksas2Queue() {
        return new Queue("teksas2Queue", false);
    }

    @Bean
    Queue teksas3Queue() {
        return new Queue("teksas3Queue", false);
    }

    @Bean
    Queue allQueue() {
        return new Queue("allQueue", false);
    }

    @Bean
    Queue fanoutQueue() {
        return new Queue("fanout", false);
    }

    @Bean
    TopicExchange topicExchange() {
        return new TopicExchange("topic-exchange");
    }

    @Bean
    Binding teksas1Binding(Queue teksas1Queue, TopicExchange topicExchange) {
        return BindingBuilder.bind(teksas1Queue).to(topicExchange).with("queue.teksas1");
    }

    @Bean
    Binding teksas2Binding(Queue teksas2Queue, TopicExchange topicExchange) {
        return BindingBuilder.bind(teksas2Queue).to(topicExchange).with("queue.teksas2");
    }

    @Bean
    Binding teksas3Binding(Queue teksas3Queue, TopicExchange topicExchange) {
        return BindingBuilder.bind(teksas3Queue).to(topicExchange).with("queue.teksas3");
    }

    @Bean
    Binding allBinding(Queue allQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(allQueue).to(topicExchange).with("queue.*");
    }

    @Bean
    Binding fanoutBinding(Queue fanoutQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(fanoutQueue).to(topicExchange).with("#");
    }

}
