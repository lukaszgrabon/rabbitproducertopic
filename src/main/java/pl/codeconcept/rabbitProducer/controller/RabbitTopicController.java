package pl.codeconcept.rabbitProducer.controller;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RabbitTopicController {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @GetMapping("/topic")
    public String produce(@RequestParam("exchangeName") String exchange, @RequestParam("routingKey") String routingKey,
                          @RequestParam("messageData") String messageData) {

        amqpTemplate.convertAndSend(exchange, routingKey, messageData);
        return "Topic";
    }
}
